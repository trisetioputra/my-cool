from django import forms

class formulir(forms.Form):
    nama_matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))
    jumlah_sks = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True
    }))
    ruang_kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Ruang Kelas',
        'type' : 'text',
        'required' : True
    }))
    deskripsi_matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsi Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))
    nama_dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required' : True
    }))
    tahun_semester = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Tahun Semester',
        'type' : 'text',
        'required' : True
    }))
