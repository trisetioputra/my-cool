from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def jadwal(request):
    if(request.method == "POST"):
        VarA = forms.formulir(request.POST)
        if(VarA.is_valid()):
            VarB = models.Jadwal()
            VarB.nama_matkul = VarA.cleaned_data["nama_matkul"]
            VarB.jumlah_sks = VarA.cleaned_data["jumlah_sks"]
            VarB.ruang_kelas = VarA.cleaned_data["ruang_kelas"]
            VarB.deskripsi_matkul = VarA.cleaned_data["deskripsi_matkul"]
            VarB.nama_dosen = VarA.cleaned_data["nama_dosen"]
            VarB.tahun_semester = VarA.cleaned_data["tahun_semester"]
            VarB.save()
        return redirect("/")
    else:
        VarA = forms.formulir()
        VarB = models.Jadwal.objects.all()
        VarA_dictio = {
            'formulir' : VarA,
            'jadwal' : VarB
        }
        return render(request, 'jadwal.html', VarA_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        VarA = forms.formulir(request.POST)
        if(VarA.is_valid()):
            VarB = models.Jadwal()
            VarB.nama_matkul = VarA.cleaned_data["nama_matkul"]
            VarB.jumlah_sks = VarA.cleaned_data["jumlah_sks"]
            VarB.ruang_kelas = VarA.cleaned_data["ruang_kelas"]
            VarB.deskripsi_matkul = VarA.cleaned_data["deskripsi_matkul"]
            VarB.nama_dosen = VarA.cleaned_data["nama_dosen"]
            VarB.tahun_semester = VarA.cleaned_data["tahun_semester"]
            VarB.save()
        return redirect("/")
    else:
        models.Jadwal.objects.filter(pk = pk).delete()
        VarA = forms.formulir()
        VarB = models.Jadwal.objects.all()
        VarA_dictio = {
            'formulir' : VarA,
            'jadwal' : VarB
        }
        return render(request, 'tabel.html', VarA_dictio)

def tabel(request):
     VarA = forms.formulir()
     VarB = models.Jadwal.objects.all()
     VarA_dictio = {
         'formulir' : VarA,
         'jadwal' : VarB
     }
     return render(request,'tabel.html',VarA_dictio)
