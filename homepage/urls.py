from django.urls import path
from . import views

appname='jadwal'
urlpatterns = [
    path('', views.jadwal,name='jadwal'),
    path('<int:pk>/',views.delete,name='hapus'),
    path('tabel/',views.tabel,name='tabel'),
]

